open Mu

let mu_executor file ast = 

  let compute_storage env (x:muOp) = List.mapi (fun i x -> x,i) (might_columns env x) in 

  let remap = Hashtbl.create 1703 in
  let rev_map = Hashtbl.create 1703 in
  let cur_map = ref 0 in

  let do_map x =
    try
      Hashtbl.find remap x
    with Not_found ->
      incr cur_map  ;
      Hashtbl.add remap x (!cur_map) ;
      Hashtbl.add rev_map (!cur_map) x;
      !cur_map
  in

  let read_file file when_add when_end =
    let nb_cols = 2 in
    let chan = open_in file in
         begin
           try
             while true; do
               let x = Array.make nb_cols 0 in
               let line = input_line chan in
               let rec foo beg len id =
                 if id<nb_cols
                 then
                   if beg+len=String.length line
                   then (x.(id) <- do_map (String.sub line beg len))
                   else
                     if line.[beg+len] = ' '
                     then (x.(id) <- do_map (String.sub line beg len) ; foo (beg+len+1) 0 (id+1))
                     else (foo beg (len+1) id)
               in
               foo 0 0 0 ;
               when_add x
             done; 
           with End_of_file ->
             close_in chan ; when_end ()
         end
  in
  

  let do_rev_map x =
     Hashtbl.find rev_map x
  in

  let print_sol name format x =
    print_string (name^" ") ;
    Array.iteri (fun i x -> if (x<>0) then (print_string (fst (List.nth format i)) ; print_string "=" ; print_string (do_rev_map x) ; print_string " ")) x ; print_string "\n"
  in

  
  let rec compil env when_end when_add ast = match ast with 
    | RenamedVar(x,l) when String.sub x 0 5 ="_pred"  ->
       let on_start () =
         read_file ("pred/"^(String.sub x 5 (String.length x - 5))) when_add when_end
         (* let nb_cols = 2 in *)
         (* let chan = open_in ("pred/"^(String.sub x 5 (String.length x - 5))) in *)
         (* begin *)
         (*   try *)
         (*     while true; do *)
         (*       let x = Array.make nb_cols 0 in *)
         (*       let line = input_line chan in *)
         (*       let rec foo beg len id = *)
         (*         if id<nb_cols *)
         (*         then *)
         (*           if beg+len=String.length line *)
         (*           then (x.(id) <- do_map (String.sub line beg len)) *)
         (*           else *)
         (*             if line.[beg+len] = ' ' *)
         (*             then (x.(id) <- do_map (String.sub line beg len) ; foo (beg+len+1) 0 (id+1)) *)
         (*             else (foo beg (len+1) id) *)
         (*       in *)
         (*       foo 0 0 0 ; *)
         (*       when_add x *)
         (*     done;  *)
         (*   with End_of_file -> *)
         (*     close_in chan ; when_end () *)
         (* end *)
       in
       ["block",(on_start,fun x->())]

    (* | Filter (RenamedVar("all",l),Equal(Evar col,Ecst va)) when col="_pred" -> *)
    (*    failwith "All required Not " *)
    (*    let on_start () =  *)
    (*      let nb_cols = 3 in *)
    (*      let chan = open_in ("pred/"^va) in *)
    (*      begin *)
    (*        try *)
    (*          while true; do *)
    (*            let x = Array.make nb_cols 0 in *)
    (*            let line = input_line chan in *)
    (*            let rec foo beg len id = *)
    (*              if id<nb_cols *)
    (*              then *)
    (*                if beg+len=String.length line *)
    (*                then (x.(id) <- do_map (String.sub line beg len)) *)
    (*                else *)
    (*                  if line.[beg+len] = ' ' *)
    (*                  then (x.(id) <- do_map (String.sub line beg len) ; foo (beg+len+1) 0 (id+2)) *)
    (*                  else (foo beg (len+1) id) *)
    (*            in *)
    (*            foo 0 0 0 ; *)
    (*            when_add x  *)
    (*          done;  *)
    (*        with End_of_file -> *)
    (*          close_in chan ; when_end () *)
    (*      end *)
    (*    in *)
    (*    ["block",(on_start,fun x->())] *)
       
    | Nodes _ ->
       failwith "All required but not implemented"
       (* let children = Sys.readdir "pred/" in *)
       (* Array.iter  children;; *)
    | RenamedVar("all",l) -> failwith "All required but not implemented"
    | Filter(muop,Equal(Evar r,Ecst s)) ->
       (* print_string r ; print_newline(); *)
       let sub_format = compute_storage env muop in
       (* let format = compute_storage env ast in *)
       let id_r = List.assoc r sub_format  in
       let s_id = do_map s in
       let sub_add x = if x.(id_r) = s_id then when_add x in
       compil env when_end sub_add muop
       
    | Join(ast_a,ast_b)
      | LeftJoin(ast_a,ast_b,True) ->
       let fa = compute_storage env ast_a in
       let fb = compute_storage env ast_b in
       

       let sure_cols_join = inter (sure_columns env ast_a) (sure_columns env ast_b) in

       let hash_a = List.map (fun x -> List.assoc x fa) sure_cols_join
       and hash_b = List.map (fun x -> List.assoc x fb) sure_cols_join in

       let do_hash a v =
         let rec foo r = function
           | [] -> r
           | a::q -> foo (71*r+Hashtbl.hash v.(a)) q
         in
         foo 41 a
       in
       
       let check =
           (List.map (fun (x,y) -> y,List.assoc x fb) (List.filter (fun (x,y) -> List.mem_assoc x fb) fa))
       in
       
       let compat x y = 
         List.for_all (fun (u,v) -> x.(u)=y.(v) || x.(u)=0 || y.(v) = 0) check
       in

       let to_merge = Array.of_list (List.map
                                       (fun x -> (if List.mem_assoc x fa then List.assoc x fa else -1),
                                                 (if List.mem_assoc x fb then List.assoc x fb else -1))
                                       (might_columns env ast)) in
       
       let merge x y =
         Array.map (fun (u,v) -> if u<0 then y.(v) else if v<0||y.(v)=0 then x.(u) else y.(v)) to_merge 
       in
       
       begin
         match ast with
         | Join(_,_) ->
            
            let a = Hashtbl.create 1703 
            and b = Hashtbl.create 1703 in
            
            let is_end = ref false in
            let subend () = if !is_end then (Hashtbl.reset a; Hashtbl.reset b;when_end ()) else is_end:=true in
            let addA v =
              (* print_sol "A" fa v ; *)
              let hash = do_hash hash_a v in
              if not (!is_end)
              then
                begin
                  match Hashtbl.find_opt a hash
                  with
                  | Some l -> Hashtbl.replace a hash (v::l)
                  | None -> Hashtbl.add a hash [v]
                end ;
              try
                List.iter (fun x -> if compat v x then when_add (merge v x)) (Hashtbl.find b hash)
              with Not_found -> ();
            in
            let addB v =
              (* print_sol "B" fb v ; *)
              let hash = do_hash hash_b v in
              if not (!is_end)
              then
                begin
                  match Hashtbl.find_opt b hash
                  with
                  | Some l -> Hashtbl.replace b hash (v::l)
                  | None -> Hashtbl.add b hash [v]
                end ;
              try
                List.iter (fun x -> if compat x v then when_add (merge x v)) (Hashtbl.find a hash) ;
              with Not_found -> () ;
            in
            
            (compil env subend addA ast_a)@(compil env subend addB ast_b)
            
         | LeftJoin(_,_,True) ->
            
            let a = Hashtbl.create 17 
            and b = Hashtbl.create 17 in

            let complete x =
              Array.map (fun (u,_) -> if u<0 then 0 else x.(u)) to_merge
            in
            
            let is_end = ref false in
            let subend () =
              if !is_end
              then
                begin
                  Hashtbl.reset b;
                  Hashtbl.iter (fun _ (x,matched) -> if not !matched then when_add (complete x)) a ;
                  Hashtbl.reset a;
                  when_end ()
                end
              else is_end:=true
            in
            
            let addA v =
              (* print_sol "A" fa v ; *)
              let hash = do_hash hash_a v in
              let matched = ref false in
              List.iter (fun x -> if compat v x then (matched:=true; when_add (merge v x))) (Hashtbl.find_all b hash) ;
              Hashtbl.add a hash (v,matched)
            in
            
            let addB v =
              (* print_sol "B" fb v ; *)
              let hash = do_hash hash_b v in
              Hashtbl.find_all a hash |>
                List.iter (fun (x,matched) ->
                    if compat x v
                    then
                      begin
                        matched:= true ;
                        when_add (merge x v)
                      end
                  ) ;
              Hashtbl.add b hash v
            in
            
            (compil env subend addA ast_a)@(compil env subend addB ast_b)
         | _ -> failwith ("match error"^__LOC__)
       end
    | Union(ast_a,ast_b) ->
       let fa = compute_storage env ast_a in
       let fb = compute_storage env ast_b in
       let subend =
         let is_end = ref false in
         fun () -> if !is_end then when_end () else is_end:=true
       in
       if fa <> fb 
       then
         let f = might_columns env ast in
         let mapA = Array.of_list (List.map (fun x -> if List.mem_assoc x fa then List.assoc x fa else -1) f)
         and mapB = Array.of_list (List.map (fun x -> if List.mem_assoc x fb then List.assoc x fb else -1) f)
         in
       
         let addA v =
           (* print_sol "A" fa v ; *)
           when_add (Array.map (fun x -> if x<0 then 0 else v.(x) ) mapA)
         in
         let addB v =
           (* print_sol "B" fb v ; *)
           when_add (Array.map (fun x -> if x<0 then 0 else v.(x) ) mapB)
         in
         (compil env subend addA ast_a)@(compil env subend addB ast_b)
       else
         (compil env subend when_add ast_a)@(compil env subend when_add ast_b)

    | Rename(m,r,r2) ->
       failwith "Renaming" 
       (* compil env when_end when_add m *)


    | Remove(muop,r) ->
       let sub_format = compute_storage env muop in
       let format = compute_storage env ast in
       let nb_cols = List.length format in
       let id_r = List.assoc r sub_format  in
       let sub_add x =
         let a=
           (Array.init nb_cols (fun i-> if i>=id_r then x.(i+1) else x.(i)))
         in
         (* print_sol "rem" format a ; *)
         when_add a
       in
       compil env when_end sub_add muop

    | RenamedVar(v,l) ->
       [v,(when_end,when_add)]

    | Iter(x,form) ->

       let senv = (x,columns env ast)::env in
       (* let format = compute_storage senv form in *)

       let rdo_var = ref [] in
       let running = ref false in
       let nextrun_todo = ref [] in
       let add =
         let seen = Hashtbl.create 1017 in 
         
         fun x ->
         if not (Hashtbl.mem seen x)
         then
           begin
             Hashtbl.add seen x () ;
             nextrun_todo := x::!nextrun_todo ;
             if not (!running)
             then
               begin
                 while !nextrun_todo <> [] do
                   running := true ;
                   let to_do = !nextrun_todo in
                   nextrun_todo := [] ;
                   List.iter when_add to_do ;
                   List.iter (fun (wa,m) ->
                              List.iter m to_do ) (!rdo_var) ;
                   running := false ;
                 done ;
               end
           end
       in
       let (bound_var,ot_var) = List.partition (fun (k,v) -> k=x) (compil senv when_end add form) in
       let nb_inside_ot_var = List.length ot_var in
       (* print_string "Number of use: "; print_int nb_inside_var ; print_newline(); *)
       let seen_end = ref nb_inside_ot_var in
       let end_var () =
         decr seen_end;
         if !seen_end<0
         then
           List.iter (fun (wa,m) -> wa()) (!rdo_var)
       in
       rdo_var := List.map snd bound_var ; 
       (* print_string "RDO_VARS "; print_int (List.length (!rdo_var)) ; print_string "\n" ; *)
       (List.map (fun (x,y) -> x,(end_var, fun _ -> ())) ot_var)@ot_var
       
    | Dup(m,r,r') ->
       let sub_format = compute_storage env m in
       let format = compute_storage env ast in
       let nb_cols = List.length format in
       
       let mapar = Array.of_list (List.map (fun (k,i) -> List.assoc (if k = r' then r else k ) sub_format) format) in
       let sub_add x = (* print_sol "dup" (format) (Array.init nb_cols (fun i-> x.(mapar.(i)))) ; *) when_add (Array.init nb_cols (fun i-> x.(mapar.(i)))) in
       compil env when_end sub_add m
  in

  let basic_env = get_basic_env ast in

  let input =
    compil basic_env
      (fun _ -> ())
      (fun x -> print_sol "SOL" (compute_storage basic_env ast) x)
      ast
  in
  List.iter (fun (s,(w_e,w_a)) ->
      match s with
      | "block" -> w_e()
      | "singleton" -> w_a (Array.make 0 0) ; w_e ()
      | _ -> failwith "free var")
            input
;;
