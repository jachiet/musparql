type pat =
  | Exact of string
  | Var of string
  | Star of pat
  | Plus of pat
  | Concat of pat list
  | Or of pat list
  | Reverse of pat
;;

type sparqlVar = string ;;

type tp = (pat*pat*pat) ;;

type query =
  | Join of query * query
  | Union of query * query
  | Optional of query * query
  | Tp of tp
  | Empty
type select_query = string list * query ;;
type prefixed_query = (string*string) list * select_query ;;
    
