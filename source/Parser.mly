%{
  open Ast
%}

%token <string> VAR
%token <string> IDENT
%token EOF
%token PREFIX
%token LEFTPAR RIGHTPAR LEFTPROG RIGHTPROG LEFTBRACKET RIGHTBRACKET
%token SELECT WHERE UNION OPTIONAL DISTINCT
%token POINT COMMA CONCAT STAR PLUS REVERSE BAR

%start query

%type <Ast.prefixed_query> query
%%
query:
| pre = list(prefix) s=selectclause EOF
    { pre,s }
;

prefix:
| PREFIX s=IDENT LEFTPROG v=IDENT RIGHTPROG
    {(s,v)}
;				     
  
selectclause:
| SELECT DISTINCT? l = separated_list(COMMA, var) WHERE c=toplevel
    { (l,c) }
| SELECT DISTINCT? l = nonempty_list(var) WHERE c=toplevel
    { (l,c) }
| SELECT DISTINCT? STAR WHERE LEFTBRACKET c = toplevel RIGHTBRACKET
    { (["*"],c) }
;

var:
| s = VAR
     { s }
;
  
ident_or_var:
| s = IDENT
   { Exact(s) }
| s = VAR
   { Var(s) }
| LEFTPROG s = VAR RIGHTPROG
   { Exact(s) }
;  

unambiguous_regexp:
| s = IDENT
   { Exact(s) }
| s = VAR
   { Var(s) }
| LEFTPAR s=regexp RIGHTPAR
   { s }

regexp:
| s = unambiguous_regexp
    { s }
| a = unambiguous_regexp BAR l=separated_list(BAR,unambiguous_regexp)
    { Or(a::l) }
| s = unambiguous_regexp STAR
    { Star(s) }
| s= unambiguous_regexp PLUS
    { Plus(s) }
| a = unambiguous_regexp CONCAT l=separated_list(CONCAT,unambiguous_regexp)
    { Concat(a::l) }
| REVERSE a=regexp
    { Reverse(a) }
;


simple_toplevel:
| LEFTBRACKET RIGHTBRACKET
  { Empty }
| LEFTBRACKET t=toplevel RIGHTBRACKET
  {t}
| a = cond
  { a }
| a = endcond
  { a }
| a = cond b = simple_toplevel
  { Join(a,b)}

toplevel:
| a = simple_toplevel UNION b=simple_toplevel
  { Union(a,b) }
| a = simple_toplevel OPTIONAL b=simple_toplevel
  { Optional(a,b) }
| s=simple_toplevel
  {s}
;

cond:
| e=endcond POINT
   { e }
;

endcond:
| sub = ident_or_var pred = regexp obj = ident_or_var
   { Tp(sub,pred,obj) }
;

