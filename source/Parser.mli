
(* The type of tokens. *)

type token = 
  | WHERE
  | VAR of (string)
  | UNION
  | STAR
  | SELECT
  | RIGHTPROG
  | RIGHTPAR
  | RIGHTBRACKET
  | REVERSE
  | PREFIX
  | POINT
  | PLUS
  | OPTIONAL
  | LEFTPROG
  | LEFTPAR
  | LEFTBRACKET
  | IDENT of (string)
  | EOF
  | DISTINCT
  | CONCAT
  | COMMA
  | BAR

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val query: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Ast.prefixed_query)
