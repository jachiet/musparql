type record_id = string 

type var_id = string 

type expr =
  | Evar of record_id
  | Ecst of string
            
type filterCond =
  |  Equal of expr * expr
  | True
            
type muOp =
  | Filter of muOp*filterCond
  | Join of muOp*muOp
  | LeftJoin of muOp*muOp*filterCond
  | Union of muOp*muOp
  | Rename of muOp*record_id*record_id
  | Remove of muOp*record_id
  | RenamedVar of var_id*(record_id*record_id) list
  | Iter of var_id*muOp
  | Nodes of record_id 
  | Dup of muOp*record_id*record_id
  
let get_basic_env t =
  let preds = ref [] in
  let isSingleton = ref false in
  let rec foo = function
    | Filter(t,_) -> foo t
    | Join(a,b) -> foo a ; foo b
    | LeftJoin(a,b,_) -> foo a ; foo b
    | Union(a,b) -> foo a; foo b
    | Rename(t,_,_) -> foo t
    | Remove(t,_) -> foo t
    | RenamedVar(v,_) ->
       if String.length v > 5 && String.sub v 0 5 = "_pred" && not (List.mem v !preds)
       then preds := v::!preds ;
       if v = "singleton"
       then  isSingleton := true 
    | Iter(_,t) -> foo t
    | Nodes(_) -> ()
    | Dup(t,_,_) -> foo t
  in
  foo t ;
  (if !isSingleton then ["singleton",([],[])] else [])@
    (!preds |>
    List.map (fun x-> x,(["_subj";"_obj"],["_subj";"_obj"])))
  
let exprPrinter = function
  | Ecst s -> "\""^s^"\""
  | Evar s -> "?"^s
         
let condPrinter = function
  | True -> "True"
  | Equal(a,b) -> exprPrinter a ^ " = " ^ exprPrinter b

let muPrinter d =
  let rec foo ident = function
    | Filter(sub,cond) -> ident^"Filter (" ^condPrinter cond^")\n"^(foo (ident^"  ") sub)
    | Join(a,b) ->
       let nid = ident^"  " in
       (foo nid a)^ident^"JOIN\n"^(foo nid b)
    | LeftJoin(a,b,cond) ->
       let nid = ident^"  " in
       (foo nid a)^ident^"OPT ("^condPrinter cond^")"^(foo nid b)
    | Union(a,b) ->
       let nid = ident^"  " in
       (foo nid a)^ident^"UNION\n"^(foo nid b)
    | Rename(d,r,n) ->
       ident^"ρ("^r^"→"^n^")\n"^(foo (ident^"  ") d)
    | Remove(d,r) ->
       ident^"π("^r^")\n"^(foo (ident^"  ") d)
    | RenamedVar(x,l) ->
       ident^x^(List.fold_left (fun ac (x,y) -> (if ac = "[" then "[" else (ac^","))^x^"->"^y) "[" l)^"]\n"
    | Iter(x,form) -> ident^"LET REC "^x^" =\n"^(foo (ident^"  ") form)
    | Nodes(l) -> ident^"Nodes"^ l^"\n"
    | Dup(t,a,b) -> ident^"β("^a^" → "^b^")\n"^(foo (ident^"  " ) t)
  in
  print_string "\n          -----------------------------------------   \n";
  print_string (foo "" d) ;
  print_newline ()




  
let try_assoc v a =
  try
    List.assoc a v
  with
    Not_found -> a


let inter l l' =
  List.filter (fun x -> (List.mem x l')) l


let diff l l' =
  List.filter (fun x -> not (List.mem x l')) l


let union l l' =
  l@(diff l' l)


let rec replace on nn = function
  | [] -> []
  | a::q -> if a = on then nn::q else a::(replace on nn q)


let rec remove s = function
  | [] -> []
  | a::q -> if a = s then q else a::(remove s q)


let rec remove_all_assoc k = function
  | [] -> []
  | (k',v)::q -> if k=k' then remove_all_assoc k q else (k',v)::remove_all_assoc k q

let rec isSubset l1 l2 =
  List.for_all (fun x -> List.mem x l2) l1
               
  
let rec equal l l' =
  diff l l' = [] && List.length l = List.length l' 

let rec apply f (a,b) =
  (f a, f b)
  
let rec expr_columns = function
  | Evar s -> [s]
  | Ecst s -> []
  
let rec filter_columns = function
  | True -> []
  | Equal(a,b) -> union (expr_columns a) (expr_columns b)

let rec columns env = function
  | Filter(sub,cond) -> columns env sub
  | Join(a,b) ->
     let (sa,ma) = columns env a
     and (sb,mb) = columns env b in 
     (union sa sb, union ma mb)
  | Union(a,b) ->
     let (sa,ma) = columns env a
     and (sb,mb) = columns env b in 
     (inter sa sb, union ma mb)
  | LeftJoin(a,b,_) ->
     let (sa,ma) = columns env a
     and (sb,mb) = columns env b in 
     (sa, union ma mb)
  | Rename(a,old_name,new_name) ->
     columns env a |>
       apply (replace old_name new_name)   
  | Remove(a,l) ->
     columns env a |>
     apply (List.filter (fun x -> x<>l))
  | RenamedVar(x,l) ->
     begin
       try  
         List.assoc x env |>
           apply (List.map (try_assoc l))
       with
       |  Not_found ->
           print_string (x^" not in [") ;
           List.iter (fun (c,j) -> print_string (c^" ")) env ;
           print_string "]\n" ;
           failwith "Not found in environment"
     end
  | Iter(x,form) ->
     let rec gets old_s m = 
       let (new_s,new_m) = columns ((x,(old_s,m))::env) form in
       if isSubset old_s new_s
        then (old_s,m)
       else gets new_s m
     in
     
     let rec getm old_m = 
       let (_,new_m) = columns ((x,([],old_m))::env) form in
       if isSubset new_m old_m
       then gets old_m old_m
       else getm new_m
     in
     getm []
  | Nodes l -> ([l],[l])
  | Dup(t,a,b) ->
     columns env t |>
     apply (fun s -> 
         if List.mem a s
         then b::s
         else s) 

  
(* Returns the possible columns of the muOp given *)
let might_columns env t =
  (* print_string "----ENV\n"; *)
  (* List.iter (fun (x,v) -> print_string x) env; *)
  (* print_string "ENV----\n"; *)
  snd (columns env t)

  
let sure_columns env t = fst (columns env t)
     

(* Compose p(t(x)) *)
let compose p t =
  union (List.map fst p) (List.map fst t) |>
    List.map (fun x -> x,try_assoc t x) |>
    List.map (fun (x,y) -> x,try_assoc p y) 


    


let rec degree x = function
  | Filter(a,c) -> degree x a
  | Join(a,b) -> (degree x a) + (degree x b)
  | LeftJoin(a,b,c) -> (degree x a) + 2* (degree x b)
  | Union(a,b) -> max (degree x a) (degree x b)
  | Rename(t,a,b) -> degree x t
  | Remove(t,col) -> degree x t
  | RenamedVar(v,_) -> if x=v then 1 else 0
  | Iter(v,t) -> 2*(degree x t)
  | Dup(t,a,b) -> degree x t
  | Nodes l -> 0
                
let linear x t = (degree x t) = 1

let constant x t = (degree x t) = 0

let rec strictlyLinear x = function (* Suppose a term of degree = 1 *)
  | Filter(a,c) -> strictlyLinear x a
  | Join(a,b) -> strictlyLinear x a || strictlyLinear x b
  | LeftJoin(a,b,c) -> strictlyLinear x a
  | Union(a,b) -> strictlyLinear x a && strictlyLinear x b
  | Rename(t,a,b) -> strictlyLinear x t
  | Remove(t,col) -> strictlyLinear x t
  | RenamedVar(v,_) -> v=x
  | Iter(v,t) -> false
  | Dup(t,a,b) -> strictlyLinear x t
  | Nodes l -> false
             
let perm x =
 
  let rec foo = function
    | Filter(t,c) -> foo t
    | Join(a,b) -> union (foo a) (foo b)
    | LeftJoin(a,b,c) -> foo a
    | Union(a,b) -> union (foo a) (foo b)
    | Rename(t,a,b) -> foo t |> List.map (compose [a,b]) 
    | Remove(t,c) -> foo t |> List.map (compose [c,""]) 
    | RenamedVar(v,l) -> if v=x then [l] else []
    | Iter(v,form) -> []
    | Nodes l -> []
    | Dup(t,a,b) -> foo t |> List.map (compose [a,b])
  in

  foo 


let destabilizer x t =
  perm x t |>
    List.map (List.filter (fun (x,y) -> x!= y))  |>
    List.map (List.map fst) |>
    List.fold_left union []

(* let _ = perm "x" (Remove(Filter(Join(Rename(Var "x","b","m"),Nodes("b")),True),"m")) *)

let canAdd x c =
  let rec foo = function
    | Filter(t,cond) -> (not (List.mem c (filter_columns cond))) &&
                          foo t
    | Join(a,b) -> foo a && foo b
    | LeftJoin(a,b,cond) -> (not (List.mem c (filter_columns cond))) &&
                           foo a && foo b
    | Union(a,b) -> foo a && foo b
    | Rename(t,a,b) -> (c!=a) && (c!= b) && foo t
    | Remove(t,a) -> (c!=a) && foo t
    | RenamedVar(v,l) -> List.for_all (fun (y,z) -> (z<>c) && (y<>c||x<>v)) l
    | Iter(v,form) -> foo form
    | Nodes l -> c!=l
    | Dup(t,a,b) -> c!=a && c!=b && foo t
  in
  foo

(* Removes all rename in a muOp *)
     
let rec rename_expr f = function
  | Ecst s -> Ecst s
  | Evar s -> Evar (f s)

let rec rename_cond f = function
  | Equal(a,b) -> Equal(rename_expr f a, rename_expr f b)
  | True -> True

let fresh =
  let rec i = ref 0 in
  fun () -> incr i ; !i
                     
              
let rec push_rename = function
  | Remove(a,r) -> Remove(push_rename a,r)
  | Union(a,b) -> Union(push_rename a,push_rename b)
  | Join(a,b) -> Join(push_rename a, push_rename b)
  | LeftJoin(a,b,c) -> LeftJoin(push_rename a,push_rename b,c)
  | Filter(a,c) -> Filter(push_rename a,c)
  | Rename(q,a,b) ->
     let q = push_rename q in 
     let f x = if x = a then b else x in
     let rec rec_apply = function
       | Join(a,b) -> Join(rec_apply a,rec_apply b)
       | LeftJoin(a,b,cond) -> LeftJoin(rec_apply a,rec_apply b,rename_cond f cond)
       | Union(a,b) -> Union(rec_apply a, rec_apply b)
       | Rename(t,x,y) ->
          print_string "===============================================\n";
          print_string "Culprit:\n";
          muPrinter t ;
          print_string "Inside :\n";
          muPrinter q ;
          failwith ("Recursive rename @ "^__FILE__^(string_of_int __LINE__))
       | Remove(t,r) ->
          if r == b
          then Remove(push_rename (Rename(t,b,"tmp_"^(string_of_int (fresh()))) |> rec_apply), f r)
          else Remove(rec_apply t, f r)
       | RenamedVar(v,l) ->
          (* print_string "===============================================\n"; *)
          (* print_string "Before push rename:\n"; *)
          (* muPrinter (Rename(RenamedVar(v,l),a,b)) ; *)
          (* print_string "After push rename:\n"; *)
          (* muPrinter (RenamedVar(v,compose [a,b] l)) ; *)

          RenamedVar(v,compose [a,b] l)
       | Filter(a,cond) -> Filter(rec_apply a, cond)
       | Iter(x,form) ->

          let rec counter_replace_vars = function
            | Remove(t,c) -> Remove(counter_replace_vars t,c)
            | Union(a,b) -> Union(counter_replace_vars a, counter_replace_vars b)
            | Join(a,b) -> Join(counter_replace_vars a, counter_replace_vars b)
            | LeftJoin(a,b,c) -> LeftJoin(counter_replace_vars a,b,c)
            | Filter(t,c) -> Filter(counter_replace_vars t, c)
            | Rename(t,a,b) -> Rename(counter_replace_vars t, a, b)
            | Iter(x,f) -> Iter(x,f)
            | Dup(t,a,b) -> Dup(counter_replace_vars t,a, b)
            | RenamedVar(v,l) ->
               RenamedVar(v,if v=x then compose l [b,a]  else l)
            | Nodes l -> Nodes l
          in 
          
          if canAdd b x form && canAdd a x form 
          then Iter(x, Rename(form  |> counter_replace_vars,a,b)|>push_rename )
          else Rename(Iter(x,form),a,b) (* TODO not completely supported *)
       | Dup(t,a,b) -> Dup(rec_apply t,f a,f b)
       | Nodes l -> Nodes (f l)
     in
     rec_apply (push_rename q)
  | Iter(x,form) ->
     Iter(x, push_rename form)  
  | Dup(t,a,b) -> Dup(push_rename t,a,b)
  | RenamedVar (x,l) -> RenamedVar(x,l)
  | Nodes l -> Nodes l
 
let gen_variation env = function
  | Remove(Remove(a,r1),r2) -> [Remove(Remove(a,r2),r1)]
  | Remove(Union(a,b),r) -> [Union(Remove(a,r),Remove(b,r))]
  | Remove(Iter(x,Union(a,b)),r) ->
     let recf,con = match (degree x a,degree x b) with
       | (0,1) -> b,a
       | (1,0) -> a,b
       | _ -> raise Not_found
     in
     let d = destabilizer x recf in
     if strictlyLinear x recf && canAdd x r recf && not (List.mem r d) 
     then [Iter(x,Union(Remove(con,r),recf))]
     else []
            
  | Join(Iter(x1,Union(a1,b1)),Iter(x2,Union(a2,b2)))  ->
     begin
       try
         let rec deep_replace = function
           | Remove(t,c) -> Remove(deep_replace t,c)
           | Union(a,b) -> Union(deep_replace a, deep_replace b)
           | Join(a,b) -> Join(deep_replace a, deep_replace b)
           | LeftJoin(a,b,c) -> LeftJoin(deep_replace a,b,c)
           | Filter(t,c) -> Filter(deep_replace t, c)
           | Rename(t,a,b) -> Rename(deep_replace t, a, b)
           | Iter(x,f) -> Iter(x,f)
           | Dup(t,a,b) -> Dup(deep_replace t,a, b)
           | RenamedVar(v,l) ->  RenamedVar((if v = x2 then x1 else v),l)
           | Nodes l -> Nodes l
         in
         
         let rec1,con1 = match (degree x1 a1,degree x1 b1) with
           | (0,1) -> b1,a1
           | (1,0) -> a1,b1
           | _ -> raise Not_found
         and rec2,con2 = (match (degree x2 a2,degree x2 b2) with
                          | (0,1) -> b2,a2
                          | (1,0) -> a2,b2
                          | _ -> raise Not_found ) |> apply deep_replace
         in
         (* print_string "PREJOINING !\n"; *)
         (* print_string "REC2:\n"; *)
         (* muPrinter (rec2); *)
         (* muPrinter (rec1); *)
         if not (strictlyLinear x1 rec1 && strictlyLinear x1 rec2 )
         then raise Not_found ; (* TODO handle general decomposition *)
         let (s1,m1) = columns env con1
         and (s2,m2) = columns env con2 in
         let (r_s1,r_m1) = columns ((x1,(s1,m1))::env) rec1
         and (r_s2,r_m2) = columns ((x1,(s2,m2))::env) rec2 in
         let com = inter m1 m2 in
         (* print_string "PREJOINING ["; *)
         (* List.iter print_string m1 ; *)
         (* print_string ","; *)
         (* List.iter print_string m2 ; *)
         (* print_string "] :"; *)
         (* List.iter print_string com ; *)
         (* print_string "\n"; *)
         let d1 = destabilizer x1 rec1
         and d2 = destabilizer x1 rec2 in
         (* [equal m1 s1 ; equal m1 r_s1 ; equal m1 r_m1 ; *)
         (*       equal m2 s2 ; equal m2 r_s2 ; equal m2 r_m2 ; *)
         (*        List.for_all (fun c -> not (List.mem c d1 || List.mem c d2)) com  ; *)
         (*        List.for_all (fun c -> canAdd x1 c rec1) (diff m2 m1 )  ; *)
         (*        List.for_all (fun c -> canAdd x1 c rec2) (diff m1 m2)] |> *)
         (*   List.iter (fun x -> print_string (if x then  "1 " else "0 "))  ; *)
         (* print_string "\n"; *)
         if equal m1 s1 && equal m1 r_s1 && equal m1 r_m1 &&
              equal m2 s2 && equal m2 r_s2 && equal m2 r_m2 &&
                List.for_all (fun c -> not (List.mem c d1 || List.mem c d2)) com  &&
                  List.for_all (fun c -> canAdd x1 c rec1) (diff m2 m1 )  &&
                    List.for_all (fun c -> canAdd x1 c rec2) (diff m1 m2)
         then  ((* print_string "JOINING! ["; *)
           (*    List.iter print_string d1 ; print_string ","; *)
           (*    List.iter print_string d2 ; print_string "]"; *)
           (*    List.iter print_string com ; *)
           (*    print_string "\n"; *)
           (*    muPrinter (Iter(x1,Union(Join(con1,con2),Union(rec1,rec2)))) ; *)
           [Iter(x1,Union(Join(con1,con2),Union(rec1,rec2)))]         )
         else raise Not_found
       with Not_found -> []
     end
       
  | Join(Iter(x,Union(a,b)),c)
  | Join(c,Iter(x,Union(a,b))) ->
     let rec1,con1 = match (degree x a,degree x b) with
       | (0,1) -> b,a
       | (1,0) -> a,b
       | _ -> raise Not_found
     in
     let m = might_columns env c in
     let s = sure_columns env c in
     let d = destabilizer x rec1 in
     let t = might_columns env (Iter(x,Union(a,b)))
     and ts = sure_columns env (Iter(x,Union(a,b))) in
     if isSubset m s && isSubset t ts &&
          (List.for_all (fun c -> not (List.mem c d) && (List.mem c t || canAdd x c (Union(a,b))) ) m)
     then [Iter(x,Union(Join(con1,c),rec1))]
     else (
     (*   [isSubset m s; isSubset t ts]@ *)
     (*     (List.map (fun c -> not (List.mem c d)) m)@ *)
     (*     (List.map (fun c ->(List.mem c t || canAdd x c rec1) ) m) *)
     (* |>  *)
     (*   List.iter (fun x-> print_string (if x then "O" else "N")) ; *)
     (*   List.iter print_string m ; *)
       [])
            
            
  | Join(Iter(x,form),a) ->
     let m = might_columns env a in
     let s = sure_columns env a in
     let d = destabilizer x form in
     let t = might_columns env (Iter(x,form))
     and ts = sure_columns env (Iter(x,form)) in
     if isSubset m s && isSubset t ts &&
          (List.for_all (fun c -> not (List.mem c d) && (List.mem c t || canAdd x c form) ) m)
     then [Iter(x,Join(form,a))]
     else []
     
  | Join(Filter(a,c),b) ->
     if isSubset (filter_columns c) (sure_columns env a) 
     then [Filter(Join(a,b),c)]
     else []
            
  | Join(Nodes n,a) -> if List.mem n (sure_columns env a) then [a] else []
                                                                          
  | Remove(Join(a,b),r) ->
     if not (List.mem r (might_columns env a))
     then  [Join(a,Remove(b,r))]
     else
       if not (List.mem r (might_columns env b))
       then [Join(b,Remove(a,r))]
       else []

  | Join(Remove(a,c),b) ->
     if List.mem c (might_columns env b)
     then (
       (* print_string "ERREUR vars appears at two distincts places!!!" ; *)
       [])
     else [Remove(Join(a,b),c)]

  | Join(Join(a,b),c) -> [Join(Join(a,c),b);Join(Join(b,c),a)]
  (* | Union(Join(a,b),c) | Union(c,Join(a,b)) -> [Union(Join(a,c),Join(b,c))]*)
  | Join(Dup(Nodes(a),c,b),y) ->
     if c!=a then failwith "Error Nodes and Dup" ;
     let ty = might_columns env y in
     let is_a = List.mem a ty
     and is_b = List.mem b ty in
     if is_a then
       if is_b
       then []
       else [Dup(y,a,b)]
     else
       if is_b
       then [Dup(y,b,a)]
       else []         
  | Join(Dup(x,a,b),y) ->
     let ty = might_columns env y in
     if not (List.mem b ty) then [Dup(Join(x,y),a,b)] else []
  | Remove(Iter(x,form),a) ->
     if canAdd x a form && not (List.mem a (destabilizer x form))
     then [Iter(x,Remove(form,a))] (* Check ! *)
     else []

  |Filter(Join(a,b),cond) ->
    let pa = might_columns env a
    and pb = might_columns env b in
    if inter (filter_columns cond) pa = []
    then [Join(a,Filter(b,cond))]
    else
      if inter (filter_columns cond) pb = []
      then [Join(b,Filter(a,cond))]
      else []

  |Filter(Remove(a,c),cond) ->
    if not (List.mem c (filter_columns cond))
    then [Remove(Filter(a,cond),c)]
    else []

  
  |Remove(Filter(a,cond),c) ->
    if not (List.mem c (filter_columns cond))
    then [Filter(Remove(a,c),cond)]
    else []
  |Filter(Iter(x,Union(a,b)),cond) ->
         let recf,con = match (degree x a,degree x b) with
       | (0,1) -> b,a
       | (1,0) -> a,b
       | _ -> raise Not_found
     in
     let d = destabilizer x recf in
     if strictlyLinear x recf &&
          inter (filter_columns cond) (destabilizer x recf) = []
     then [Iter(x,Union(Filter(con,cond),recf))]
     else []

  |Filter(Iter(x,form),cond) ->    
    if inter (filter_columns cond) (destabilizer x form) = []
    then [Iter(x,Filter(form,cond))]
    else (
      (* print_string "==================================================\n" ; *)
      (* print_string "Could not push filter into\n" ; *)
      (* muPrinter (Filter(Iter(x,form),cond)); *)
      [])
  | RenamedVar(x,l) -> []
  | r -> []






(* let a = RenamedVar("_predP2",["_obj","?a";"_subj","j1"]) *)
(* let b = Remove(Join( *)
(*                    RenamedVar("_predP2",["_obj","j3";"_subj","j1"]), *)
(*                    RenamedVar("v",["j1","j3"]) *)
(*                  ),"j3") *)
(* let c = Iter("v",Union(a,b)) *)
(* let d = Join(RenamedVar("_predP2",["_obj","j1"]),c) *)
(* let test_env = get_basic_env d  *)
(* let _ = gen_variation test_env d *)
(* let _ = destabilizer "v" b *)
(* let [l] = reversed_of test_env d *)
(* let _ = *)
(*   match l with *)
(*   |Join(a,Iter(v,f)) -> destabilizer "v" f *)
(* let _ = muPrinter l *)
(* let _ = gen_variation test_env l |> List.iter muPrinter *)


           

let rec normalize (env:(var_id*(var_id list* var_id list)) list) =

  let do_join a b = 
     let na = normalize env a
     and nb = normalize env b in 
     if na > nb then Join(nb,na) else Join(na,nb)
  in
  
  function
  | Filter(a,cond) -> Filter(normalize env a,cond)
  | Join(Dup(Nodes n,m,l),a) ->
     let ta = might_columns env a in
     if n==m && List.mem n ta && List.mem l ta
     then normalize env a
     else
       if n==m && List.mem n ta
       then normalize env (Dup(a,n,l))
       else
         if n==m && List.mem l ta
         then normalize env (Dup(a,l,n))
         else do_join (Dup(Nodes n,m,l)) a
  | Join(Dup(t,a,b),u)
  | Join(u,Dup(t,a,b)) ->
     if List.mem b (might_columns env u)
     then do_join u (Dup(t,a,b))
     else Dup(normalize env (Join(t,u)),a,b)
             
  | Join(Nodes(a),b)
  | Join(b,Nodes(a)) ->
     let tb = sure_columns env b in
     if List.mem a tb
     then normalize env b
     else do_join (Nodes a) b
  | Join(a,b) -> do_join a b
  | LeftJoin(a,b,cond) -> LeftJoin(normalize env a,normalize env b,cond)
  | Union(a,b) ->
     let na = normalize env a
     and nb = normalize env b in
     if na = nb
     then na
     else
       if na > nb
       then Union(nb,na)
       else Union(na,nb)
  | Remove(a,r) ->
     if List.mem r (might_columns env a)
     then Remove(normalize env a,r)
     else normalize env a
  | RenamedVar(x,l) ->
     let (s,m) = List.assoc x env in
     (* print_string (x) ; *)
     (* List.iter (fun (x,y)-> print_string ("|"^x^"->"^y)) l; *)
     (* List.iter (fun x-> print_string ("+"^x)) m ; *)
     (* print_string "\n"; *)
     RenamedVar(x,List.filter (fun (x,y) -> x<>y && List.mem x m) l)
  (* TODO better normalization of l ?*)
  | Iter(x,f) ->
     let nenv = (x,columns env (Iter(x,f)))::env in
     Iter(x,normalize nenv  f)
  | Nodes l -> Nodes l
  | Rename(t,a,b) -> (* failwith ("Rename at "^(string_of_int __LINE__)) *)
     Rename(normalize env t,a,b)
  | Dup(t,a,b) -> Dup(normalize env(t),a,b)
 
  
                
let rec permute  = function
  | Join(a,b) -> [Join(a,b);Join(b,a)]
  | Union(a,b) -> [Union(a,b);Union(b,a)]
  | r -> [r]


let reverse env x = function
  | Union(a,b) ->
     begin
       let recf,con = match (degree x a,degree x b) with
         | (0,1) -> b,a
         | (1,0) -> a,b
         | _ -> raise Not_found     
       in
       (* print_string "REVERSING :\n" ; *)
       (* muPrinter (Iter(x,Union(recf,con))) ; *)
       let sure,might = columns env con in
       let nenv = (x,(sure,might))::env in
       let s2,m2 = columns nenv recf in
       if not (equal s2 m2 && equal sure might && equal sure s2)
       then raise Not_found ;
       let colJoin,changed,add = match recf with
         | Remove(Join(RenamedVar(v,[changed,colJoin2]),add),colJoin)
              when colJoin2=colJoin && v=x ->
            colJoin, changed,add
         | Remove(Join(add,RenamedVar(v,[changed,colJoin2])),colJoin)
              when colJoin2=colJoin && v=x ->
            colJoin, changed,add
         | _ -> raise Not_found
       in
       (* add has to be constant in x since whole term is linear *)
       
       (* ot_col is the column of recf that is not modified at each turn *)  
       let ot_col = match sure with
         | [x;y] -> if x=changed then y else x
         | _ -> raise Not_found
       in
       (* print_string "============ ALMOST REVEEEEEEEERSSSED ========\n" ; *)
       let tmp = "tmp"^__FILE__ in
       let add_simp_large = (Rename(Rename(Rename(add,ot_col,tmp),colJoin,ot_col),tmp,colJoin)) in
       (* let add_simp_large = (Rename(Rename(Rename(add,a,tmp),colJoin,ot_col),tmp,colJoin) in *)
       let add_simp = normalize env (push_rename add_simp_large) in
       (* muPrinter add_simp_large ; *)
       (* muPrinter con ; *)
       (* muPrinter recf ; *)
       (* print_string "============ ALMOST REVEEEEEEEERSSSED 2 ========\n" ; *)
       (* muPrinter add_simp ; *)
       if add_simp = con
       then
         begin
           let t = normalize env (push_rename (Union(con,Remove(Join(Rename(add_simp,changed,colJoin),RenamedVar(x,[ot_col,colJoin])),colJoin)))) in
           (* print_string "============ REVEEEEEEEERSSSED ========\n" ; *)
           (* muPrinter t ; *)
           t
         end
       else
         begin
           match con with
           | Dup(Nodes(n),a,b) ->
              (* print_string "============ 2 REVEEEEEEEERSSSED ========\n" ; *)
              let t = Union(con,Remove(Join(add_simp,RenamedVar(x,[ot_col,colJoin])),colJoin)) in
              (* muPrinter t ; *)
              t
           | _ -> muPrinter con ;
                  muPrinter add_simp ;
                  failwith ("Reversed failed at"^__FILE__^":"^string_of_int(__LINE__))
         end
     end
       | _ ->  raise Not_found


let rec reversed_of env = function
    | Filter(m,c) -> List.map (fun x -> (Filter(x,c))) (reversed_of env m)
    | Join(a,b) ->
       let na = reversed_of env a 
       and nb = reversed_of env b in
       List.flatten (
           List.map (fun x->List.map (fun y ->Join(x,y)) nb) na
         )
    | Union(a,b) ->
       let na = reversed_of env a 
       and nb = reversed_of env b in
       List.flatten (
           List.map (fun x->List.map (fun y ->Union(x,y)) nb) na
         )
    | LeftJoin(a,b,c) ->
       let na = reversed_of env a 
       and nb = reversed_of env b in
       List.flatten (
           List.map (fun x->List.map (fun y ->LeftJoin(x,y,c)) nb) na
         )
    | Rename(t,a,b) ->
       reversed_of env t |>
         List.map (fun x -> Rename(x,a,b))
       
    | Remove(a,r) ->
       reversed_of env a |>
         List.map (fun x -> Remove(x,r))
    | RenamedVar(v,l) -> [RenamedVar(v,l)]
    | Iter(x,f) ->
       (* print_string "Try reversing !\n" ; *)
       let nenv = (x,columns env (Iter(x,f)))::env in
       ((try [reverse nenv x f]
       with Not_found -> [] )@
       reversed_of nenv f) |>
         List.map (fun newf -> Iter(x,newf))
    | Nodes(n) -> [Nodes(n)] 
    | Dup(t,a,b) ->
       reversed_of env t |>
       List.map (fun x -> Dup(x,a,b))
  

let rec gen_all_var env t =
  let foo = function
    | Filter(m,c) -> List.map (fun x -> (Filter(x,c))) (gen_all_var env m)
    | Join(a,b) ->
       List.map (fun x -> Join(x,b)) (gen_all_var env a)@
	 List.map (fun x -> Join(a,x)) (gen_all_var env b)
    | Union(a,b) ->
       List.map (fun x -> Union(x,b)) (gen_all_var env a)@
	 List.map (fun x -> Union(a,x)) (gen_all_var env b)
    | LeftJoin(a,b,c) ->
       List.map (fun x -> LeftJoin(x,b,c)) (gen_all_var env a)@
	 List.map (fun x -> LeftJoin(a,x,c)) (gen_all_var env b)
    | Rename(t,a,b) ->
       gen_all_var env t |> List.map (fun x -> Rename(x,a,b))
       
    | Remove(a,r) ->
       List.map (fun x -> Remove(x,r)) (gen_all_var env a)
    | RenamedVar(v,l) -> [RenamedVar(v,l)]
    | Iter(x,f) ->
       (* print_string "Try reversing !\n" ; *)
       let nenv = (x,columns env (Iter(x,f)))::env in
       (* (try [reverse nenv x f] *)
       (* with Not_found -> [] )@ *)
       gen_all_var nenv f |>
         List.map (fun newf -> Iter(x,newf))
    | Nodes(n) -> [Nodes(n)] 
    | Dup(t,a,b) -> List.map (fun x -> Dup(x,a,b)) (gen_all_var env t)
  in
  let new_var = foo t::(List.map (gen_variation env) (permute t)) in
  (List.flatten new_var)
  

    
module MuHash =

  struct

    type t = muOp
	       
    let equal i j = i=j

    let rec (hash :t->int) = function
      | Filter(d,c) -> 17+3*(hash d)+(Hashtbl.hash c)
      | Rename(d,r,s) -> 9+2*(hash d)+(Hashtbl.hash r)+(Hashtbl.hash s)
      | Join(a,b) -> 2+5*(hash a)+3*(hash b)
      | LeftJoin(a,b,c) -> 31+13*(hash a)+3*(hash b)+11*(Hashtbl.hash c)
      | Union(a,b)-> 111+7*(hash a)+hash b
      | RenamedVar(v,l) ->  923+Hashtbl.hash v+3*Hashtbl.hash l
      | Remove(a,l)-> 32+hash a+Hashtbl.hash l
      | Iter(v,f)->167+29*(hash f)+Hashtbl.hash v
      | Nodes l-> Hashtbl.hash l
      | Dup(t,a,b) -> 17+hash t + 3*Hashtbl.hash (a,b)
  end

    
module MuHashtbl = Hashtbl.Make(MuHash)


			       
let rec push_remove env r = function
  | Join(a,b) ->
     if List.mem r (might_columns env a)
     then
       if List.mem r (might_columns env b)
       then Remove(Join(a,b),r)
       else Join(push_remove env r a,b)
     else
       Join(a,push_remove env r b)
  | Union(a,b) ->
     if List.mem r (might_columns env a)
     then
       if List.mem r (might_columns env b)
       then Remove(Union(a,b),r)
       else Union(push_remove env r a,b)
     else
       Union(a,push_remove env r b)
  | Remove(a,v) -> Remove(push_remove env r a,v)
  | Iter(x,form) ->
     let u = destabilizer x form in
     if (not (List.mem r u))  && (canAdd x r form)
     then Iter(x,push_remove env r form)
     else Remove(Iter(x,form),r)
  | Dup(t,a,b) -> if b=r then t
                  else
                    if a=r then push_rename (Rename(t,a,b))
                    else Dup(push_remove env r t,a,b)
  | a -> Remove(a,r) (*Filter, Rename, Var*)




(* let basic_env = *)
(*   (["all",(["_subj";"_pred";"_obj"],["_subj";"_pred";"_obj"])]) *)
       
let iter = function
  (*  | Remove(a,r) -> List.map (push_remove env r) (iter a) *)
  | t ->
     let basic_env = get_basic_env t in
     let ht = MuHashtbl.create 17000 in     
     let uniq l =
       let addtest x =
	 let nx = normalize basic_env x in
	 (not (MuHashtbl.mem ht nx)) && (MuHashtbl.add ht nx ();true)
       in
       List.filter addtest l
     in
     
     let rec foo seen t res =

       (* print_string "ITERATION with " ; print_int (List.length t) ; print_newline() ; *)
       (*  muPrinter (normalize basic_env (List.hd t )); *)
       let n =
         List.map (gen_all_var basic_env) t |>
           List.flatten |>
           uniq |>
           List.map (normalize basic_env) in
       let seen = List.length n + seen in
       if seen < 3000
       then
         let nouv = List.map (normalize basic_env) n in
         if nouv = []
         then res
         else foo (seen) nouv (nouv@res)
       else res
     in
     foo 0
     ((reversed_of basic_env t))
       []


let rec find_needle = function
  | Union(a,b)  | Join(a,b) -> find_needle a || find_needle b
  | Remove (a,r) -> find_needle a
  | Iter("var_4",form) ->
     begin
       let rec foo3 = function
         | Join(a,b) | Union(a,b) -> foo3 a || foo3 b
         | Filter(a,c) -> true
         | _ -> false
       in
       let rec foo2 = function
         | Join(a,b) | Union(a,b) -> foo2 a || foo2 b
         | Remove(a,r) -> foo3 a
         | _ -> false
       in
       let rec foo = function 
         | Union(a,b)  | Join(a,b) -> foo a || foo b
         | Remove (a,r) -> foo a
         | Iter("var_1",form) ->
            foo2 form
         | _ -> false
       in
       foo form
     end
  | _ -> false

       
(* TODO fix reverse*)

