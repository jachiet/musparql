open Mu ;;

type stat_col = (string*int) list ;;
  
let stat = function
  | "all","all" -> 100000.
  | "any","any" -> 1000.
  | "pred","LeSuperPrefixis" -> 10.
  | "pred","LeSuperPrefixknows" -> 20.
  | _ -> 100.
;;

let stat_obj = function
  | "pred/LeSuperPrefixis","human" -> 10.
  | "pred/LeSuperPrefixknows","bob" -> 100.
  | _ -> 100.
;;
  
let cost =

  let rec foo env = function
    | Filter(Nodes l,c) ->
       (* let (c,s) = foo env (ReadFile(a,l)) in *)
       (* (c,stat_obj (a,v)) *)
       (0.,1.)
    | Filter(RenamedVar("all",l),(Equal(Evar v,Ecst c))) ->
       (* let (c,s) = foo env (ReadFile(a,l)) in *)
       (* (c,stat_obj (a,v)) *)
       let s = stat (c,v) in
       (s,s)
    (* | Filter(ReadFile(a,l),c,v) -> *)
    (*    let (c,s) = foo env (ReadFile(a,l)) in *)
    (*    (c,stat_obj (a,v)) *)
    | Filter(Iter(x,f),c) ->
       let (c,s) = foo env (Iter(x,f)) in
       (c+.1100000000.,s*.0.8)
    | Filter(a,c) ->
       let (c,s) = foo env a in
       (c,s*.0.8)
    | Remove(a,b) ->
       let (c,s) = foo env a in
       (c,s)
    (* | ReadFile(a,b) -> *)
    (*    let s = stat a in (1.,s) *)
    | LeftJoin(a,b,c) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       (ca+.cb+.sa*.sb,sa*.sb)
    | Join(a,b) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       let ta = sure_columns env a
       and tb = sure_columns env b in
       let tj = Mu.inter ta tb in
       if tj = []
       then (ca+.cb+.sa*.sb,sa*.sb)
       else
	 if max (List.length ta) (List.length tb) = List.length tj
	 then (ca+.cb+.sa+.sb,max sa sb)
	 else
           let size_common = (List.length tj) |> float_of_int in
           let size_ab = List.length ta+List.length tb |> float_of_int in
	   let rat =  (size_ab-.size_common*.2.) /. size_ab in
	   let newsize = (max sa sb) *. ((min sa sb) ** rat)  in
	   (ca+.cb+.newsize,newsize)
    | Union(a,b) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       (ca+.cb+.sa+.sb,sa+.sb)
    (* | RenamedVar(x,l) -> (if x = "all" then (stat ("any","any")) else 10000.),1000. *)
    | RenamedVar(x,l) -> (if String.sub x 0 5 = "_pred" then 100. else 10000.),1000.
    | Iter(x,f) ->
       let (c,s) = foo  ((x,columns env (Iter(x,f)))::env) f in
       let sure_cols = sure_columns env (Iter(x,f)) in
       let cs =  sure_cols
                 |> List.length
                 |>  float_of_int in
       let destab = destabilizer x f |> inter sure_cols in
       let ds = destab |> List.length |> float_of_int in
       let penalty = 1000. *. 1000. *. 1000. *. 1000. *.(ds)*.(cs) in
       if ds +. cs < 2.1
       then
         (c+.1000.*.1000.,1000.*.1000.)
       else
         (c+.penalty*.10.,s+.penalty )
    | Nodes l -> 1000000000.,1000000000.
    | Rename(t,a,b) ->
       foo env t
       (* failwith ("Rename at "^(string_of_int __LINE__)) *)
    | Dup(t,a,b) -> let (c,s)= foo env t in (c+.s,s)
  in
  fun x -> fst (foo (Mu.get_basic_env x) x)
;;

let costDebug =

  let rec foo env = function
    | Filter(Nodes l,c) ->
       (* let (c,s) = foo env (ReadFile(a,l)) in *)
       (* (c,stat_obj (a,v)) *)
       (0.,1.)
    | Filter(RenamedVar("all",l),(Equal(Evar v,Ecst c))) ->
       (* let (c,s) = foo env (ReadFile(a,l)) in *)
       (* (c,stat_obj (a,v)) *)
       let s = stat (c,v) in
       (s,s)
    (* | Filter(ReadFile(a,l),c,v) -> *)
    (*    let (c,s) = foo env (ReadFile(a,l)) in *)
    (*    (c,stat_obj (a,v)) *)
    | Filter(Iter(x,f),c) ->
       let (c,s) = foo env (Iter(x,f)) in
       (c+.1100000000.,s*.0.8)
    | Filter(a,c) ->
       let (c,s) = foo env a in
       (c,s*.0.8)
    | Remove(a,b) ->
       let (c,s) = foo env a in
       (c,s)
    (* | ReadFile(a,b) -> *)
    (*    let s = stat a in (1.,s) *)
    | LeftJoin(a,b,c) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       (ca+.cb+.sa*.sb,sa*.sb)
    | Join(a,b) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       let ta = sure_columns env a
       and tb = sure_columns env b in
       let tj = Mu.inter ta tb in
       if tj = []
       then (ca+.cb+.sa*.sb,sa*.sb)
       else
	 if max (List.length ta) (List.length tb) = List.length tj
	 then (ca+.cb+.sa+.sb,max sa sb)
	 else
           let size_common = (List.length tj) |> float_of_int in
           let size_ab = List.length ta+List.length tb |> float_of_int in
	   let rat =  (size_ab-.size_common*.2.) /. size_ab in
	   let newsize = (max sa sb) *. ((min sa sb) ** rat)  in
           let _ =
             print_string "cost,size of join \n" ;
             muPrinter (Join(a,b)) ;
             print_string "is " ;
             print_float (ca+.cb+.newsize);
             print_string " , " ;
             print_float (newsize);
             print_newline()
           in
	   (ca+.cb+.newsize,newsize)
    | Union(a,b) ->
       let ca,sa = foo env a
       and cb,sb = foo env b in
       (ca+.cb+.sa+.sb,sa+.sb)
    (* | RenamedVar(x,l) -> (if x = "all" then (stat ("any","any")) else 10000.),1000. *)
    | RenamedVar(x,l) -> (if String.sub x 0 5 = "_pred" then 100. else 10000.),1000.
    | Iter(x,f) ->
       let (c,s) = foo  ((x,columns env (Iter(x,f)))::env) f in
       let sure_cols = sure_columns env (Iter(x,f)) in
       let cs =  sure_cols
                 |> List.length
                 |>  float_of_int in
       let destab = destabilizer x f |> inter sure_cols in
       let ds = destab |> List.length |> float_of_int in
       let penalty = 1000. *. 1000. *. 1000. *. 1000. *.(ds)*.(cs) in
       (c+.penalty,s+.penalty )
    | Nodes l -> 1000000000.,1000000000.
    | Rename(t,a,b) ->
       foo env t
       (* failwith ("Rename at "^(string_of_int __LINE__)) *)
    | Dup(t,a,b) -> let (c,s)= foo env t in (c+.s,s)
  in
  fun x -> let _ = (foo (Mu.get_basic_env x) x) in () 
;;
