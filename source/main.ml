open Lexer ;;
open Parser ;;
open SparqlToMu ;;
open Mu ;;
open CostAst ;;
open SparkPrinter ;;
open MuExecutor ;;

let time f x =
  let t = Sys.time() in
  let fx = f x in
  Printf.printf "execution time: %f\n" ((Sys.time() -. t)*.1000.);
  fx
  
let _ =
  let file = ref "req" in

  if (Array.length Sys.argv) < 1
  then failwith "Not enough args!" ;
  file := Sys.argv.(1) ;
    
  let debug = ref false
  and verbose = ref false
  and stats = ref false
  and execute = ref false
  and parse = ref false
  and showmu = ref false
  and timing = ref false in
  for i = 2 to Array.length Sys.argv - 1 do
    if Sys.argv.(i) = "parse" then parse := true ;
    if Sys.argv.(i) = "debug" then debug := true ;
    if Sys.argv.(i) = "stats" then stats := true ;
    if Sys.argv.(i) = "showmu" then showmu := true ;
    if Sys.argv.(i) = "execute" then execute := true ;
    if Sys.argv.(i) = "verbose" then verbose := true ;
    if Sys.argv.(i) = "timing" then timing := true ;
  done ;
  try
    if !stats then  print_string ("Opening file "^(!file)^"!\n") ;
    let c = open_in (!file) in
    let lb = Lexing.from_channel c in
    let prefix,(varlist,conds) =  (Parser.query Lexer.next_token lb) in
    let fstAst = SparqlToMu.in_to_ast prefix conds varlist in
    if !debug || !parse then (print_string "Parsed AST is:\n"; muPrinter fstAst );
    if !parse then exit 0 ;
    let renAst =  push_rename fstAst in
    if !debug then (print_string "The push-renamed AST is:\n" ; muPrinter renAst ) ;
    let renAst =  Mu.normalize (Mu.get_basic_env renAst) renAst in
    if !debug then (print_string "The normalized push-renamed AST is:\n" ; muPrinter renAst ) ;
    let allAst = iter renAst in
    let c_best,bestAst = List.fold_left (fun ac el -> min (cost el,el) ac) (cost renAst, renAst) allAst in
    (* let c_ast = cost renAst in *)
    if !verbose
    then
      begin
        allAst |>
          (* List.filter find_needle        |> *)
	List.iter (fun x -> if true || cost x < c_best*.20. then (if !debug then costDebug x ; muPrinter x; print_newline(); print_string "^---- Cost is " ;print_float (cost x);print_newline()))  ; 
	print_newline () ;
      end ;
    if !stats then (print_string "Number of AST considered : " ; print_int ( List.length allAst) ; print_newline ()) ;
    if !showmu then (print_string "Estimated best ast (" ; print_float c_best ; print_string ") is \n" ; if !debug then costDebug bestAst ; muPrinter bestAst) ;

    (* if !spark then mu_to_spark (!file) bestAst ; *)
    if !execute then (if !timing then time else fun f x-> f x) (mu_executor !execute) bestAst  ;
  with
  | Lexer.Lexing_error s ->
     print_string ("lexical error: "^s);
     exit 1
  | Parser.Error ->
     print_string ("Parsing error (around line "^(string_of_int (!Lexer.line))^")\n");
     exit 1
;;
