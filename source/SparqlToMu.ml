open Mu ;;
open Ast ;;

let varall = Mu.RenamedVar("all",[])

  
let in_to_ast rep_list cond var = 

  let fresh =
    let id = ref 0 in
    fun s -> (incr id; s^"_"^string_of_int (!id))
  in
  
  let replace s (p,v) =
    let rec foo i =
      if String.length p = i
      then v^(String.sub s i (String.length s - i))
      else
	if String.length s>i && p.[i] = s.[i]
	then foo (i+1)
	else s
    in
    foo 0
  in

  let replace_all a = List.fold_left replace a rep_list in

  let compile_regexp a  =
    let rec foo = function
      | Reverse(a) ->
         Rename(Rename(Rename(foo a,"_obj","_tempRev"),"_subj","_obj"),"_tempRev","_subj")
      | Concat(a::l) ->
         if l = []
         then foo a
         else 
	   let join_var = fresh "join" in
	   Remove(Join(Rename(foo a,"_obj",join_var),Rename(foo (Concat l),"_subj",join_var)),join_var)
         
         
      | Or(a::l) ->
         if l = [] || List.mem a l 
         then foo a
         else Union(foo a, foo (Or l))
      | Star(a) ->
	 let x = fresh "var" in
	 let j = fresh "join" in
	 Iter(x,Union(Remove(Join(RenamedVar(x,["_subj",j]),Rename(foo a,"_obj",j)),j), Dup(Nodes "_subj","_subj","_obj")))
      | Exact a -> Mu.RenamedVar("_pred"^replace_all a,[])
         (* Remove(Filter(varall,Equal(Evar "_pred", Ecst (replace_all a))),"_pred") *)
      | Var a -> Rename(varall,"_pred",a)
      | Plus(a) ->
         let x = fresh "var" in
	 let j = fresh "join" in
         Iter(x,Union(Remove(Join(RenamedVar(x,["_subj",j]),Rename(foo a,"_obj",j)),j),foo a))
      (* | _ -> failwith ("Unhandled regexp  "^__FILE__^" "^(string_of_int __LINE__)) *)
 
    in
    foo a
  in
  
  let combine name v s = match v with
    | Exact a ->  Remove(Filter(s,Equal(Evar name,Ecst (replace_all a))),name)
    | Var(a) -> Rename(s,name,a)
    | a ->
       if name <> "_pred" then failwith "Unsupported regexp for non predicate" ;
       Join(compile_regexp a,s)
  in
  
  let cond_to_ast = function
    | (a,Exact(b),c) ->
       Mu.RenamedVar("_pred"^replace_all b,[])
       (* Remove(Filter(varall,Equal(Evar "_pred",Ecst (replace_all b))),"_pred") *)
       (* ReadFile("pred/"^(replace_all b),["_subj";"_obj"]) *)
       |> combine "_subj" a
       |> combine "_obj"  c
    (* | (a,b,Exact(c)) -> *)
    (*    ReadFile("obj/"^(replace_all c),["_subj";"_pred";"_obj"]) *)
    (*    |> combine "_pred"  b *)
    (*    |> combine "_subj" a *)
    (* | (Exact(a),b,c) -> *)
    (*    ReadFile("subj/"^(replace_all a),["subj";"_pred";"_obj"]) *)
    (*    |> combine "_pred" b *)
    (*    |> combine "_obj"  c *)
    | (a,b,c) ->
       compile_regexp b
       |> combine "_subj" a
       |> combine "_obj"  c
  in

  let rec foo = function
    | Ast.Join(a,b) -> Mu.Join(foo a,foo b)
    | Ast.Union(a,b) -> Mu.Union(foo a, foo b)
    | Ast.Optional(a,b) -> Mu.LeftJoin(foo a, foo b,True)
    | Ast.Tp(tp) -> cond_to_ast tp
    | Ast.Empty -> Mu.RenamedVar("singleton",[])
  in

  let a = foo cond in
  let basic_env = Mu.get_basic_env a in
  let t = might_columns basic_env a in
  if var = ["*"]
  then a
  else
    let rem_var = diff t var in
    List.fold_left (fun ac el -> Remove(ac,el)) a rem_var

;;
