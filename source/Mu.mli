type record_id = string
type var_id = string

type expr =
  | Evar of record_id
  | Ecst of string
            
type filterCond =
  |  Equal of expr * expr
  | True
            
type muOp =
  | Filter of muOp*filterCond
  | Join of muOp*muOp
  | LeftJoin of muOp*muOp*filterCond
  | Union of muOp*muOp
  | Rename of muOp*record_id*record_id
  | Remove of muOp*record_id
  | RenamedVar of var_id*(record_id*record_id) list
  | Iter of var_id*muOp
  | Nodes of record_id 
  | Dup of muOp*record_id*record_id


val get_basic_env : muOp -> (record_id * (record_id list*record_id list)) list
val muPrinter : muOp -> unit
val normalize :  (record_id * (record_id list*record_id list)) list -> muOp -> muOp
val iter : muOp -> muOp list
val push_rename : muOp -> muOp 
val sure_columns : (record_id*(record_id list*record_id list)) list -> muOp -> record_id list
val might_columns :  (record_id*(record_id list*record_id list)) list ->  muOp -> record_id list
val columns :   (record_id*(record_id list*record_id list)) list -> muOp ->  record_id list*record_id list
val inter : record_id list -> record_id list -> record_id list
val diff : record_id list -> record_id list -> record_id list
val find_needle : muOp -> bool
val destabilizer : var_id -> muOp -> record_id list
